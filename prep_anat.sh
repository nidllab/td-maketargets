#!/bin/bash
# fast normalization pipeline

base_path=/output
subj=$1
mkdir -p ${base_path}/derivatives/sub-${subj}/ants
cd ${base_path}/derivatives/sub-${subj}/ants

cp ${FSLDIR}/data/standard/MNI152_T1_2mm_brain.nii.gz .

antsRegistration --dimensionality 3 --float 0 \
  --output [MNI_to_T1_,MNI_in_T1_warped.nii.gz] \
  --interpolation Linear \
  --winsorize-image-intensities [0.005,0.995] \
  --use-histogram-matching 0 \
  --initial-moving-transform [T1_brain.nii.gz,MNI152_T1_2mm_brain.nii.gz,1] \
  --transform Rigid[0.1] \
  --metric MI[T1_brain.nii.gz,MNI152_T1_2mm_brain.nii.gz,1,32,Regular,0.25] \
  --convergence [500x250,1e-6,10] \
  --shrink-factors 8x4 \
  --smoothing-sigmas 4x2vox \
  --transform Affine[0.1] \
  --metric MI[T1_brain.nii.gz,MNI152_T1_2mm_brain.nii.gz,1,32,Regular,0.25] \
  --convergence [500x250,1e-6,10] \
  --shrink-factors 8x4 \
  --smoothing-sigmas 4x2vox \
  --transform SyN[0.1,3,0] \
  --metric CC[T1_brain.nii.gz,MNI152_T1_2mm_brain.nii.gz,1,4] \
  --convergence [50x20,1e-6,10] \
  --shrink-factors 8x4 \
  --smoothing-sigmas 3x2vox \
  -a 1

# warp atlas
for i in 1 2; do
antsApplyTransforms -d 3 \
  -i /scripts/templates/tms_indexed_${i}.nii.gz \
  -r T1_brain.nii.gz \
  -o tms_${i}_in_T1.nii.gz \
  -n NearestNeighbor \
  -t MNI_to_T1_Composite.h5
done
