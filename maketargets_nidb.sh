#!/bin/bash
uid=$1
subj=$2

DICOMDIR=~/Experiments/Hancock_TD/in/dicoms/${subj}
mkdir -p $DICOMDIR
scp -r auto_voxel@psypacs.psy.uconn.edu:/data1/nidb/archive/${uid}/1/* $DICOMDIR

# because NiDB is horrid
mv $DICOMDIR/*/dicom/*.dcm $DICOMDIR/


docker run -it --rm -v ~/Experiments/Hancock_TD/in:/input -v ~/Experiments/Hancock_TD/out:/output rhancock/maketargets $subj

open ~/Experiments/Hancock_TD/out
open ~/Experiments/Hancock_TD/in/bx
