# Automatic TMS Targets

This repository contains build files for Docker container that will create TMS targets from the TD music and prosody fMRI data.


# Usage

## Pull data
After the fMRI tasks have been run, have the data pushed from the console. The MRS shimming can be done first. 

TBD

## Copy behavioral PsychoPy log files

Copy the behavioral log files from the stimulus computer to `${HOME}/HancockTD/in` on the calculation machine. Filenames must match the BIDS-style names of fMRI data (e.g. `sub-PILOT01_task-music_run-01.csv`)


## Create Targets

```
docker run -it --rm -v ${HOME}/HancockTD/in:/input -v ${HOME}/HancockTD/out:/output -v ${HOME}/HancockTD/td-maketargets:/scripts maketargets /scripts/prep_tms.sh SUBJECTID
```

## Transfer session

1. Copy the directory `${HOME}/HancockTD/out/derivatives/sub-SUBJECTID/tms` to a USB drive.
2. In Localite, load the session directory you just copied

The following steps still need to be completed in Localite

- LPA/RPA/Nasion definition and registration
- Brain segmentation/peeling
- Entry calculation. Targets are already defined, but you will need to calculate an entry point for each target.

 


# Implementation Details

## Preprocessing

1. DICOM to BIDS using bidskit
2. Timing files to BIDS event files (`bx/parse_mdt_log.py`)
3. BIDS event files to FSL files (`bids2fslevents.py`)
4. A coarse non-linear warp from 2mm MNI space to the T1 is calculated using ANTs (as in optivox)
5. Warp is applied to the ROI restrictions in `templates/`
3. First level FEAT analysis
4. Second level FEAT
5. Threshold of Beat contrast at Z>2.3
6. Find CoG of surviving Z stat in each ROI, or fallback to CoG of the ROI
6. Generate Localite XML

## Target search restriction

The CoG of thresholded activation within each anatomical ROI is used as a target. The search restriction masks are created by `templates/make_roi.sh` and include right hemisphere

- pSTG (Harvard-Oxford)
- PMC (BA6, Juelich)
- IFG (BA44/45, Juelich)
- PF (PF*, Juelich)
- Precentral gyrus (BA4a/p, Juelich)

Search masks are constrained to be within 10mm of the brain outline.

