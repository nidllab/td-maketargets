#!/bin/bash
# create targets based on ROIs from the prep script
zstat=$1
outdir=$2
target_radius=2

tdir=`mktemp -d`

for thresh in 2.3 3; do
	fslmaths $zstat -thr $thresh ${tdir}/zthresh

	for roi in templates/label_*.nii.gz; do
		res=(`fslstats ${tdir}/zthresh -k $roi -C -v | sed -e 's/\.[0-9]*/ /g'`)

		# if there are no surviving voxels in the ROI, take the COG of the ROI mask
		if [ ${res[3]} -eq 0 ]; then
			res=(`fslstats $roi -C | sed -e 's/\.[0-9]*/ /g'`)
			echo "WARNING: no activation in $roi at Z=$thresh. Using mask COG."
		fi

		# make target ROI
		fname="${tdir}/target_z${thresh}_"`basename $roi`
		fslmaths ${FSLDIR}/data/standard/MNI152_T1_1mm_brain_mask.nii.gz -roi \
			$(( ${res[0]} - $target_radius )) $(( $target_radius * 2)) \
			$(( ${res[1]} - $target_radius )) $(( $target_radius * 2)) \
			$(( ${res[2]} - $target_radius )) $(( $target_radius * 2)) \
			0 1 $fname -odt float

	done
	fslmerge -t ${tdir}target_z${thresh}4d ${tdir}/target_z${thresh}*.nii.gz
	fslmaths ${tdir}target_z${thresh}4d -Tmax -bin ${outdir}/targets_z${thresh} -odt float

done

rm -rf $tdir
