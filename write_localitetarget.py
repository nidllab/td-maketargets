#!/usr/bin/env python
# create the EntryTarget.xml file
import xml.etree.ElementTree as ET
import copy
import numpy as np
import pandas as pd
from scipy import ndimage
import nibabel as nib
import argparse

argparser = argparse.ArgumentParser(description='Create an EntryTarget.xml file')
argparser.add_argument('--stat', type=str, nargs='+', help='Path to unthresholded stat')
argparser.add_argument('--masks', type=str, nargs='+', help='Path to an indexed mask image')
argparser.add_argument('--labels', type=str, nargs='+', help='Path to labels for the masks')
argparser.add_argument('--threshold', type=float, default=2.3, help='Stat threshold')
argparser.add_argument('--outxml', type=str, help='Path to the output XML file')
argparser.add_argument('--outtsv', type=str, help='Path to the output table')

args = argparser.parse_args()
assert(len(args.stat)==len(args.masks)==len(args.labels))

def get_coordinates(stat_path, roi_path, threshold=2.3):
    """Get center of mass cordinates within indexed ROIs.
    """

    stat = nib.load(stat_path)
    stat_data = stat.get_data()
    thresholded = stat_data * (stat_data > threshold)
    roi = nib.load(roi_path)
    roi_data = roi.get_data()
    vox_coords = ndimage.center_of_mass(thresholded, roi_data, 1.+np.arange(roi_data.max()))

    trans = stat.affine[:3, 3]
    ras_coords = np.asarray(vox_coords) + trans
    return ras_coords


targets = pd.DataFrame()

for i in range(len(args.stat)):
    ras_coords = get_coordinates(args.stat[i], args.masks[i], args.threshold)
    labels = pd.read_table(args.labels[i], header=None, names=['label'])
    df = pd.concat([labels, pd.DataFrame(ras_coords,
                        columns=['x', 'y', 'z'])], axis=1)
    targets = targets.append(df)


colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b']
targets['color'] = colors[0:targets.shape[0]]
targets.to_csv(args.outtsv, sep='\t')
# create xml
root = ET.Element("EntryTargetList")
root.attrib['coordinateSpace'] = "RAS"

rotation = ET.Element("Rotation")
rotation.attrib ={'angle': "0.0", 'used': "false"}
rotationReference = ET.SubElement(rotation, 'RotationReference')
rotation_colvec = ET.SubElement(rotationReference, 'ColVec3D')
rotation_colvec.attrib = {'data0': '0.0', 'data1': '0.0', 'data2': '-1.0'}

for i in range(targets.shape[0]):
    row = targets.iloc[i]
    colvec = {'data0': str(row['x']), 'data1': str(row['y']),
              'data2': str(row['z'])}
    tag_dict = {'alwaysVisible': "false", 'index': str(i),
                'selected': "false", 'skip': "false"}
    if i == 0:
        tag_dict['selected'] = 'true'
    target = ET.SubElement(root, 'Target')
    target.attrib = tag_dict
    target_marker = ET.SubElement(target, 'Marker')
    target_marker.attrib = {'color': row['color'], 'description': row['label'],
                            'drawAura': "true", 'radius': "4.0", 'set': "true"}
    target_coord = ET.SubElement(target_marker, 'ColVec3D')
    target_coord.attrib = colvec

    entry = ET.SubElement(root, 'Entry')
    entry.attrib = tag_dict
    entry_marker = ET.SubElement(entry, 'Marker')
    entry_marker.attrib = {'color': '#00ff00', 'description': row['label'],
                           'drawAura': "false", 'radius': "4.0", 'set': "false"}
    entry_coord = ET.SubElement(entry_marker, 'ColVec3D')
    entry_coord.attrib = colvec

    thisRotation = copy.deepcopy(rotation)
    thisRotation.attrib['index'] = str(i)
    root.append(thisRotation)

tree = ET.ElementTree(root)
tree.write(args.outxml, xml_declaration=True, encoding='UTF-8')
