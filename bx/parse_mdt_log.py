#!/usr/bin/env python
import scipy.io as io
import pandas as pd
import numpy as np
import argparse
argparser = argparse.ArgumentParser(description="Create a BIDS-style events.tsv file from a PsychoPy log")
argparser.add_argument("infile", help="Input tsv file")
argparser.add_argument("prefix", help="Prefix for the output .tsv file")
args = argparser.parse_args()

trials = pd.read_csv(args.infile, sep='\t')
keep_columns = ['trial_type', 'onset', 'duration']

trials = trials[trials['trial_type'].notnull()]
blocks = trials[keep_columns]

# responses
trials = trials[trials['response'].notnull()]
trials.loc[trials['response']==1, 'trial_type'] = 'leftbutton'
trials.loc[trials['response']==2, 'trial_type'] = 'leftbutton'

trials.loc[trials['response']==4, 'trial_type'] = 'rightbutton'
trials.loc[trials['response']==3, 'trial_type'] = 'rightbutton'

trials['onset'] = trials['response_time']
trials['duration'] = 0
responses = trials[keep_columns]

df = blocks.append(responses).sort_values('onset')

df.to_csv(args.prefix + '.tsv', sep='\t', index=False, doublequote=False)
