#!/usr/bin/env python
import pandas as pd
import numpy as np
import argparse
argparser = argparse.ArgumentParser(description="Create a BIDS-style events.tsv file from a PsychoPy log")
argparser.add_argument("infile", help="Input csv file")
argparser.add_argument("prefix", help="Prefix for the output .tsv file")
args = argparser.parse_args()

trials = pd.read_csv(args.infile)
keep_columns = ['trial_type', 'onset', 'duration']

offset = trials['disacq_resp.rt'][0] #time of the first trigger indicating start of fMRI data

trials = trials[trials['trial.start_global'].notnull()]
trials['onset'] = trials['trial.start_global'] - offset
trials['onset'] =  trials['onset'] + 1.0 #start of sound
trials['duration'] = trials['bat_sound.duration'].combine_first(trials['mdt_sound.duration'])
trials['trial_type'] = trials['condition']

# copies
blocks = trials[keep_columns]

# responses
trials['rt'] = trials['mdt_resp.rt'].combine_first(trials['bat_resp.rt'])
trials['onset'] = trials['rt'] + trials['onset']
trials['response'] = trials['mdt_resp.keys'].combine_first(trials['bat_resp.keys'])
trials.loc[trials['response']==1, 'trial_type'] = 'leftbutton'
trials.loc[trials['response']==4, 'trial_type'] = 'rightbutton'
trials = trials[trials['response'].notnull()]
trials['duration'] = 0
responses = trials[keep_columns]

df = blocks.append(responses).sort_values('onset')

df.to_csv(args.prefix + '.tsv', sep='\t', index=False, doublequote=False)
