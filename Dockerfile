FROM neurodebian:xenial
MAINTAINER <rhancock@gmail.com>
ARG DEBIAN_FRONTEND=noninteractive

ENV LANG="C.UTF-8" \
    LC_ALL="C.UTF-8"

# apt installs
## essential packages
RUN echo "deb http://neuro.debian.net/debian data main contrib non-free" >> /etc/apt/sources.list

RUN apt-get update -y && \
	apt-get upgrade -y && \
	apt-get install -yq --no-install-recommends \
	curl wget bzip2 tar ca-certificates git ssh-client unzip tcsh python file


## FSL
WORKDIR /tmp
ENV FSLDIR /usr/local/fsl
RUN touch ~/.bashrc && touch ~/.bash_profile
RUN curl -O https://fsl.fmrib.ox.ac.uk/fsldownloads/fslinstaller.py && \
	chmod 755 fslinstaller.py && \
	./fslinstaller.py -d ${FSLDIR} -D

ENV PATH "${PATH}:${FSLDIR}/bin"
RUN . ${FSLDIR}/etc/fslconf/fsl.sh
#RUN ${FSLDIR}/etc/fslconf/fslpython_install.sh


# ROBEX
WORKDIR /usr/local
RUN curl "https://www.nitrc.org/frs/download.php/5994/ROBEXv12.linux64.tar.gz//?i_agree=1&download_now=1" | tar xz
ENV PATH="$PATH:/usr/local/ROBEX"

# ANTs
RUN apt-get install -yq --no-install-recommends ants


WORKDIR /usr/local
RUN git clone https://github.com/jmtyszka/bidskit.git
ENV PATH="$PATH:/usr/local/bidskit"


# dcm2niix
RUN curl -LO https://github.com/rordenlab/dcm2niix/releases/download/v1.0.20180622/dcm2niix_27-Jun-2018_lnx.zip  && \
	unzip dcm2niix_27-Jun-2018_lnx.zip && \
	mv dcm2niix /usr/local/bin

#python

WORKDIR /tmp
ENV PATH="/usr/local/miniconda/bin:$PATH"
RUN curl -LO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
	bash Miniconda3-latest-Linux-x86_64.sh -b -p /usr/local/miniconda && \
	conda config --add channels conda-forge && \
	conda install -y numpy nibabel pandas nipype pydicom
RUN pip install pybids


RUN mkdir /input && mkdir /output && mkdir /scripts

RUN apt-get install -yq libopenblas-dev

# Cleanup
RUN apt-get clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


ENV FSLTCLSH=${FSLDIR}/bin/fsltclsh \
	FSLWISH=${FSLDIR}/bin/fslwish \
	FSLOUTPUTTYPE=NIFTI_GZ

RUN useradd --create-home -s /bin/bash mktarget
USER mktarget

# require for feat
ENV USER=mktarget

COPY . /scripts
ENV PATH=/scripts:${PATH}

WORKDIR /tmp
ENTRYPOINT ["/scripts/run.sh"]
