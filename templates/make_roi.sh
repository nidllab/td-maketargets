#!/bin/bash
# prepare the ROI masks needed to ID TMS clusters
dim=2
juelich_atlas=${FSLDIR}/data/atlases/Juelich/Juelich-maxprob-thr50-${dim}mm.nii.gz
ho_cort_atlas=${FSLDIR}/data/atlases/HarvardOxford/HarvardOxford-cort-maxprob-thr50-${dim}mm.nii.gz
ho_sub_atlas=${FSLDIR}/data/atlases/HarvardOxford/HarvardOxford-sub-maxprob-thr0-${dim}mm.nii.gz

rm label* tms_roimask

fslmaths ${FSLDIR}/data/standard/MNI152_T1_${dim}mm_brain_mask.nii.gz -kernel sphere 10 -ero brain_mask_ero10

# BA6: 92
# BA44: 14
# BA45: 16
# PF*: 28 30 32
# BA4a/p 48, 50
# right brain: 13
# subtract eroded mask to keep COGs near the cortical surface
fslmaths $ho_sub_atlas -uthr 13 -thr 13 -bin -sub brain_mask_ero10 -bin mask_rh


fslmaths $juelich_atlas -uthr 92 -thr 92 -mas mask_rh -bin label1_PMC
fslmaths $juelich_atlas -uthr 16 -thr 14 -mas mask_rh -bin label1_IFG
fslmaths $juelich_atlas -uthr 32 -thr 28 -mas mask_rh -bin label1_PF
fslmaths $juelich_atlas -uthr 50 -thr 48 -mas mask_rh -bin label2_motor

# STGp: 10
fslmaths $ho_cort_atlas -uthr 10 -thr 10 -mas mask_rh -bin label1_STG


fslmaths ${FSLDIR}/data/standard/MNI152_T1_${dim}mm_brain_mask.nii.gz -mul 0 null

for i in 1 2; do
	ls label${i}_*.nii.gz | sed -e "s/label${i}_//" -e "s/\.nii.gz//" > labels${i}.txt
	fslmerge -t labels${i} null label${i}_*.nii.gz
	fslmaths labels${i}.nii.gz -Tmaxn -mas mask_rh tms_indexed_${i}
done

