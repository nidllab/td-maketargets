#!/bin/bash
subj=$1
ses=1
### Run BIDS conversion
echo "Creating a BIDS structure..."
mkdir -p /output/{bids,derivatives,work}
mkdir /output/derivatives/conversion
cp /scripts/resources/Protocol_Translator.json /output/derivatives/conversion

dcm2bids.py -i /input/dicoms -o /output/bids --no-sessions --overwrite

mkdir -p /output/derivatives/sub-${subj}/{events,ants,feat,targets}
mkdir /output/tms-${subj}
### Convert logs

echo "Creating timing files..."
run=1
for f in /input/bx/sub-${subj}_task-music_*.tsv; do
    run_str=`printf "%02d" $run`
    outname=`basename $f`
    /scripts/bx/parse_mdt_log.py $f /output/bids/sub-${subj}/func/sub-${subj}_task-music_run-${run_str}_events

    /scripts/bx/bids2fsl_events.py /output/bids/sub-${subj}/func/sub-${subj}_task-music_run-${run_str}_events.tsv \
    /output/derivatives/sub-${subj}/events/sub-${subj}_task-music_run-${run_str} \
    --tr 1.07 --nvol 6
done

### Preprocess anatomy

echo "Processing T1..."
# brain extraction
mkdir -p /output/derivatives/sub-${subj}/ants

runROBEX.sh /output/bids/sub-${subj}/anat/sub-${subj}_run-01_T1w.nii.gz \
/output/derivatives/sub-${subj}/ants/T1_brain.nii.gz

### Coarse ANTS NL warp to MNI
/scripts/prep_anat.sh $subj 

### First level feat models
# TODO: flexibily handle runs
# TODO: create a Nipype
# TODO: reduce number of contrasts
mkdir -p /output/derivatives/sub-${subj}/feat/music_group/stats1
rm -rf /output/derivatives/sub-${subj}/feat/music_run*.feat*
echo "Starting first level...."
rm /output/derivatives/sub-${subj}/feat/music_group/design.txt
for run in 01; do
    echo '1' >> /output/derivatives/sub-${subj}/feat/music_group/design.txt
    fsffile=/output/derivatives/sub-${subj}/feat/music_run-${run}.fsf
    cat /scripts/resources/level1.fsf | sed -e "s/SUBJ/${subj}/g" -e "s#FSLDIR#${FSLDIR}#" -e "s/RUN/${run}/g" > $fsffile
    feat $fsffile &
done
wait

echo "Starting second level...."

### Second level
# cd /output/derivatives/sub-${subj}/feat/music_group
# df=`cat /output/derivatives/sub-${subj}/feat/music_run-01.feat/stats/dof`


# for run in 01; do
# flirt -ref /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/reg/highres \
#     -in /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/mask.nii.gz \
#     -out mask_${run}_T1.nii.gz \
#     -applyxfm -init /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/reg/example_func2highres.mat \
#     -interp nearestneighbour -datatype int
# done
# fslmerge -t mask mask_${run}_T1.nii.gz
# fslmaths mask -mul $df tdof

# for cope in 1 3; do
#     for run in 01; do
#     for ft in cope varcope; do
#         flirt -ref /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/reg/highres \
#         -in /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/stats/${ft}${cope}.nii.gz \
#         -out ${ft}_${cope}_${run}_T1.nii.gz \
#         -applyxfm -init /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/reg/example_func2highres.mat \
#         -interp trilinear -datatype int
#     done

#     done

#     fslmerge -t copes${cope} cope_${cope}_*_T1.nii.gz
#     fslmerge -t varcopes${cope} varcope_${cope}_*_T1.nii.gz

#     Text2Vest design.txt design.mat
#     echo '1' > contrast.txt
#     Text2Vest contrast.txt contrast.con
#     flameo --cope=copes${cope} --vc=varcopes${cope} --dvc=tdof --mask=mask --ld=stats${cope} --dm=design.mat --cs=design.mat --tc=contrast.con  --runmode=fe
# done
run=01
for zstat in zstat1 zstat3; do #beat-melody, right-left
flirt -ref /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/reg/highres \
        -in /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/stats/${zstat}.nii.gz \
        -out /output/derivatives/sub-${subj}/feat/music_group/stats1/${zstat}.nii.gz \
        -applyxfm -init /output/derivatives/sub-${subj}/feat/music_run-${run}.feat/reg/example_func2highres.mat \
        -interp trilinear -datatype int
done


echo "Creating session...."

### Create the localite session

cp -R /scripts/resources/localite_session/* /output/tms-${subj}
mv /output/tms-${subj}/SUBJID_20000101_SUBJID_68a4e7ce /output/tms-${subj}/${subj}_20000101_${subj}_68a4e7ce
cat /scripts/resources/localite_session/PatientList.xml | sed -e "s/SUBJID/${subj}/g" > /output/tms-${subj}/PatientList.xml
cat /scripts/resources/localite_session/SUBJID_20000101_SUBJID_68a4e7ce/PatientData.xml | sed -e "s/SUBJID/${subj}/g" > /output/tms-${subj}/${subj}_20000101_${subj}_68a4e7ce/PatientData.xml
cat /output/bids/sub-${subj}/anat/sub-${subj}_run-01_T1w.nii.gz | gunzip > /output/tms-${subj}/${subj}_20000101_${subj}_68a4e7ce/BinData/NIFTI/T1w.nii
cat /output/derivatives/sub-${subj}/feat/music_group/stats1/zstat1.nii.gz | gunzip > /output/tms-${subj}/${subj}_20000101_${subj}_68a4e7ce/BinData/NIFTI/targets.nii

session_time=`date "+%Y%m%d%H%M%S.%5N%z"`
cat /scripts/resources/localite_session/SUBJID_20000101_SUBJID_68a4e7ce/Sessions/Session_Default/Session.xml | sed -e "s/SESSION_TIME/${session_time}/g" > /output/tms-${subj}/${subj}_20000101_${subj}_68a4e7ce/Sessions/Session_Default/Session.xml
/scripts/write_localitetarget.py --stat /output/derivatives/sub-${subj}/feat/music_group/stats1/zstat1.nii.gz \
    /output/derivatives/sub-${subj}/feat/music_group/stats1/zstat3.nii.gz \
--masks /output/derivatives/sub-${subj}/ants/tms_1_in_T1.nii.gz \
    /output/derivatives/sub-${subj}/ants/tms_2_in_T1.nii.gz \
--labels /scripts/templates/labels1.txt \
    /scripts/templates/labels2.txt \
--threshold 1.96 \
--outxml /output/tms-${subj}/${subj}_20000101_${subj}_68a4e7ce/Sessions/Session_Default/EntryTarget/EntryTarget.xml \
--outtsv /output/tms-${subj}/${subj}_targets.tsv

echo "Done! Copy the folder /output/tms-${subj}"
